# Freelancer Service

Freelancer Service is RESTful API application for query freelancers information from RDBMS

 Freelancer Service use following main technology
- Spring Boot
- JAX-RS, JPA
- JDBC for Postgresql, H2
- Rest Assure
- Fabric8
- ConfigMap

## Test with JUnit

This service used JUnit, Rest Assure for testing by using H2 database on memory as RDBMS

```
mvn test
```

## Running with Local Profile

This service can be run locally on your machine by following command

```
java -Dspring.profiles.active=dev -jar target/freelancer-service-1.0.0-SNAPSHOT.jar
```

Properties file for local profile located at src/main/java/resources/application-dev.properties


## Deploy to OpenShift
Create configmap by using shell scipt
```
etc/create_configmap.sh
```

Fabric8 plugin can be used to deploy to OpenShfit with S2I by
```
mvn fabric8:deploy -Popenshift
```

### API document

Following URI are supported

| Method | URI                                   |  Description                |
|--------|---------------------------------------|-----------------------------
| GET    | /freelancers                  |  get all freelancers        |
| GET    | /freelancers/{freelancerId}   |  get freelancer by ID       | 


Following data can be used for test when run locally
- Freelancer ID: 123456,234567,345678,456789



## Test on OpenShift

Following URLs are available for test Freelancer Service on OpenShift

```
curl http://freelancer-service-homework.apps.rhpds311.openshift.opentlc.com/freelancers
curl http://freelancer-service-homework.apps.rhpds311.openshift.opentlc.com/freelancers/{freelancerId}

```
Following screen shot shows data on PostgreSQL

![GitHub Logo](/images/freelancer-data.png)

## PostgreSQL CLI
Use following CLIs to connect to PostgreSQL
```
# rsh to PostgreSQL
oc rsh <PostgresSQL pod>
# Connect to database
psql sampledb dbuser
# list tables
\dt
# Query all freelancer
select * from freelancer;
# Quit from psql shell
\q

```


## Authors

* **Voravit** 
