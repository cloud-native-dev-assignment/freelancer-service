package com.example.freelancerservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.example.freelancerservice.service.FreelancerRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
//@TestPropertySource(locations = "classpath:application-test.properties")
public class FreelancerRepositoryTestCase {
    @Autowired
    private FreelancerRepository repo;

    @Test
    public void findByIdAndFound(){
        List<com.example.freelancerservice.model.Freelancer> freelancers = repo.findById(123456L);
        assertTrue(!freelancers.isEmpty());
        assertEquals("Jack", freelancers.get(0).getFirstName());
        assertEquals("Teller", freelancers.get(0).getLastName());
        assertEquals("jteller@sons.com", freelancers.get(0).getEmail());

    }

    @Test
    public void findByIdAndNotFound(){
        List<com.example.freelancerservice.model.Freelancer> freelancers = repo.findById(1L);
        assertTrue(freelancers.isEmpty());

    }

    @Test
    public void listAll(){
        List<com.example.freelancerservice.model.Freelancer> freelancers = repo.findAll();
        assertNotNull(freelancers);
    }

    
}