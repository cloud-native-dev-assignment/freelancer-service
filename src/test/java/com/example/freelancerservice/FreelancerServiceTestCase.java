package com.example.freelancerservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.example.freelancerservice.exception.FreelancerNotFound;
import com.example.freelancerservice.model.Freelancer;
import com.example.freelancerservice.service.FreelancerService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
//@TestPropertySource(locations = "classpath:application-test.properties")
public class FreelancerServiceTestCase {
    private static final long validId = 123456L;
    private static final long inValidId = 1L;
    @Autowired
    private FreelancerService freelancerService;
    @Test
    public void findByIdAndFound() throws FreelancerNotFound {
        assertNotNull(freelancerService.getById(validId));
        
    }
    @Test(expected = FreelancerNotFound.class)
    
    public void findByIdAndNotFound() throws FreelancerNotFound {
        freelancerService.getById(inValidId);
        
    }
    @Test
    public void listAll(){
        assertNotNull(freelancerService.listAll());
    }   
}