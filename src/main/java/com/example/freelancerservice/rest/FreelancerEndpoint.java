package com.example.freelancerservice.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.freelancerservice.exception.FreelancerNotFound;
import com.example.freelancerservice.model.Freelancer;
import com.example.freelancerservice.service.FreelancerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Path("/freelancers")
@Component
public class FreelancerEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(FreelancerEndpoint.class);
    @Autowired
    private FreelancerService freelancerService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(){
        logger.info("List all freelancer");
        return Response.ok(freelancerService.listAll())
					 .status(Response.Status.OK).build();
    }

    @GET
    @Path("/{freelancerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("freelancerId") Long freelancerId) {
        logger.info("Query for Freelancer ID:" + freelancerId);

        try {
            Freelancer freelancer = freelancerService.getById(freelancerId);
            return Response.ok(freelancer)
		// 				// .cacheControl(control)
					 .status(Response.Status.OK).build();
        } catch (FreelancerNotFound e) {
            return Response.status(Response.Status.NOT_FOUND).
				 entity("Not found:"+freelancerId).build();
        }
    }
    
    
}