package com.example.freelancerservice.service;

import java.util.List;

import com.example.freelancerservice.exception.FreelancerNotFound;
import com.example.freelancerservice.model.Freelancer;



public interface FreelancerService {

    Freelancer getById(Long freelancerId) throws FreelancerNotFound;
    List<Freelancer> listAll();
    
}