package com.example.freelancerservice.service;

import java.util.List;

import com.example.freelancerservice.exception.FreelancerNotFound;
import com.example.freelancerservice.model.Freelancer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FreelancerServiceImpl implements FreelancerService {

    private static final Logger logger = LoggerFactory.getLogger(FreelancerServiceImpl.class);
    
    @Autowired
    FreelancerRepository repo;

    @Override
    public Freelancer getById(Long freelancerId) throws FreelancerNotFound{
        logger.info("Query ID:"+freelancerId);
        //Hibernate.initialize(repo.findById(freelancerId));
        List<Freelancer> freelancers = repo.findById(freelancerId);
        if(!freelancers.isEmpty()){
            Freelancer freelance = freelancers.get(0);
            logger.info("Found freelancer ID: "+freelance.getId());
            return freelance;
        }
        else {
            throw new FreelancerNotFound();
        }
        
    }

    @Override
    public List<Freelancer> listAll() {
        List<Freelancer> freelancers = repo.findAll();
        return freelancers;
    }

    
}