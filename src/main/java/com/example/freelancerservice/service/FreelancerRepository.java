package com.example.freelancerservice.service;

import java.util.List;

import com.example.freelancerservice.model.Freelancer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FreelancerRepository extends JpaRepository<Freelancer, Long> {
    List<Freelancer> findById(Long id);
    //  @Query("select * from freelancer where id=:id", nativeQuery = true)
    //  public Freelancer findByFreelancerId(@Param("id") Integer id);
    //Freelancer findByFreelancerId(Integer freelancerId);
}